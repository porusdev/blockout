+++
title = "User guide"
weight = 2
description = "Test"
+++

# How do I use the extension ?

With our `instagram-blocklist-extension` you can block lists of users, and also create list of users to block, so that you can share them with your friends and block them together !

### Prerequisites

Here are some things you have to check before using the extension :

- Have installed the extension (of course 😛)
- Log into instagram in the browser

Now everything is in order ! 👍

### Blocking lists

In our website we provide a set of lists that can be found [here](/blocklists)
- First choose a list you want
- In our case we decide to take [the list for Congo](/list-for-congo.txt)
- Then we click on add `Add another block list` inside the extension
- We copy the link the to `.txt` file into our extension, and click on the `+` symbol
- And now we can see that accounts are getting blocked

![Add congo to blocking](/images/install/blocklist-congo.gif)


### Create lists

In order to create lists we can do this :
- Go on the list builder section
- Give it a name and title
- Then go on the page of the user you want to add in the list
- Then click on `Add current user`
- After adding all the users you want you can then export your list !

![Add congo to blocking](/images/install/create-list.gif)