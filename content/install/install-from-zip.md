+++
title = "Install from zip"
weight = 2
description = "Test"
+++

# (Beta version)

Although the extension isn't available yet, you can still download it by using the zip file here and load it in your **chrome** based browser.


{{< button "https://github.com/AntoninJuquel/instagram-blocklist-extension/releases/download/v0.0.2-beta/instagram-blocklist-extension.zip" "Install the extension (ZIP)" >}}

{{< tip "warning" >}}
Make sure your browser has developper mode is enabled
{{< /tip >}}

![Enable dev mode](/images/install/enable-dev-mode.gif)


Then extract the content of the `zip file`.

Then load the unpacked extension like this :

![Enable dev mode](/images/install/load-unpacked.gif)
{{< tip "info" >}}
Make sure you see the folder `assets` when you open the folder
{{< /tip >}}

Now you can start using it ! 😄