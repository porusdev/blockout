+++
title = "Extension : Instagram Blocklist"
weight = 1
description = "Test"
+++


You can download our extension right [here](install-from-zip) :

{{< button "https://chromewebstore.google.com/detail/instagram-blocklist/efghnigapnojioilokgpbeekdcpkffcn" "Install the  extension" >}}

It's compatible with most chrome based browsers, and it works also with Opera !

For further information on how to use the extension, check out [this](/install/how-to-use-the-blocker)
