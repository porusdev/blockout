+++
title = "Blocklists"
weight = 1
description = "Test"
+++

Here is the list of ALL the accounts to boycott !

{{< button "/all.txt" "Get ALL accounts list here" >}}

Copy the URL of the list into your extension so that you can block everything.

{{< tip "info" >}}
The list above is a collection of all our lists, you might want to checkout each individual lists to make sure that you want to block them.
{{< /tip >}}

### Current lists :

These are all the lists that we propose currently, and all of these lists combined are found in the list above :

- [List for Palestine 🇵🇸](list-for-palestine)
- [List for Congo 🇨🇩](list-for-congo)
- [List for Uyghurs ❤](list-for-uyghurs)
- [List for celebrities blockout ⭐📵](list-of-celebrities)
- [List for celebrities blockout ⭐📵 (In France) : blockout2024\_france](list-blockout-france)

### Have lists for other topics ?

Feel free to share that by sending it to this [form](https://docs.google.com/forms/d/e/1FAIpQLSfP9Crmf23fv0-00MuccKr7u957fedmCLRrViVzqcPSeMJPFw/viewform?usp=sf_link) !


