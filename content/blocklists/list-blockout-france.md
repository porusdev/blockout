+++
title = "Blockout List french accounts"
weight = 5
description = "Test"
[dataset1]
  fileLink = "content/list-for-blockout-fr-accounts.csv"
  colors = ["#ef7f1a", "#627c62", "#11819b", "#4e1154"] # chart colors
  columnTitles = ["User ID", "Username"]
  baseChartOn = 2
  title = "Boycott list"
+++

Here is a list made by the account : blockout2024\_france on instagram, this page has compiled a list of influencers that stayed silent during this genocide.

### Sources 

- [The instagram account "blockout2024\_france"](https://www.instagram.com/blockout2024_france)

If you have more to suggest please do so by sending me your list in that [form](https://docs.google.com/forms/d/e/1FAIpQLSfP9Crmf23fv0-00MuccKr7u957fedmCLRrViVzqcPSeMJPFw/viewform?usp=sf_link).

{{< button "/list-for-blockout-fr-accounts.txt" "Download the list" >}}

---

{{< grid "3" >}}
  {{< chart "dataset1" "table" >}}
{{< /grid >}}
