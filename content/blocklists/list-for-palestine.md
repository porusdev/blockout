+++
title = "List for Palestine"
weight = 1
description = "Test"
[dataset1]
  fileLink = "content/list-for-palestine.csv"
  colors = ["#ef7f1a", "#627c62", "#11819b", "#4e1154"] # chart colors
  columnTitles = ["User ID", "Username"]
  baseChartOn = 2
  title = "Boycott list"
+++

{{< tip "warning" >}}
The list is still under construction, we are still working on it
{{< /tip >}}


Here is the list of companies and people that should be blocked in part of the boycott in favor of Palestine !


{{< button "/list-for-palestine.txt" "Download the list" >}}

---

{{< grid "3" >}}
  {{< chart "dataset1" "table" >}}
{{< /grid >}}