+++
title = "List for Uyghurs"
weight = 3
description = "Test"
[dataset1]
  fileLink = "content/list-for-uyghurs.csv"
  colors = ["#ef7f1a", "#627c62", "#11819b", "#4e1154"] # chart colors
  columnTitles = ["User ID", "Username"]
  baseChartOn = 2
  title = "Boycott list"
+++

Here is the list of companies that are still linked to Uyghur forced labor.

There still might be some that are missing, if you have more to suggest please do so by sending it in this [form](https://docs.google.com/forms/d/e/1FAIpQLSfP9Crmf23fv0-00MuccKr7u957fedmCLRrViVzqcPSeMJPFw/viewform?usp=sf_link) !

### Sources 

- [https://www.saveuighur.org/these-brands-are-still-linked-to-uyghur-forced-labor-help-stop-them-now/](https://www.saveuighur.org/these-brands-are-still-linked-to-uyghur-forced-labor-help-stop-them-now/)

{{< button "/list-for-uyghurs.txt" "Download the list" >}}

---

{{< grid "3" >}}
  {{< chart "dataset1" "table" >}}
{{< /grid >}}
