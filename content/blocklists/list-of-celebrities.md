+++
title = "List of celebrities"
weight = 4
description = "Test"
[dataset1]
  fileLink = "content/list-against-celebs.csv"
  colors = ["#ef7f1a", "#627c62", "#11819b", "#4e1154"] # chart colors
  columnTitles = ["Username", "Name", "Followers"]
  baseChartOn = 3
  title = "Boycott list"
+++

{{< tip "warning" >}}
Be cautious this is a long list that hasn't been rectified yet ! Some false positive might be in the list.
{{< /tip >}}

# How did I get the list ?

I parsed the following list of Jimmy Fallon to find a lot of celebrities

{{< button "/list-against-celebs.txt" "Download the list" >}}

---

{{< grid "3" >}}
  {{< chart "dataset1" "table" >}}
{{< /grid >}}
