+++
title = "List for Congo"
weight = 2
description = "Test"
[dataset1]
  fileLink = "content/list-for-congo.csv"
  colors = ["#ef7f1a", "#627c62", "#11819b", "#4e1154"] # chart colors
  columnTitles = ["User ID", "Username"]
  baseChartOn = 2
  title = "Boycott list"
+++

Here is the list of things to block to support the boycott in support of Congo.

Currently I have found some main tech companies that are complicit in the child labor in cobalt mines

### Sources 

- [https://www.reuters.com/legal/us-appeals-court-dismisses-child-labor-case-against-tech-companies-2024-03-05/](https://www.reuters.com/legal/us-appeals-court-dismisses-child-labor-case-against-tech-companies-2024-03-05/)

If you have more to suggest please do so by sending me your list in that [form](https://docs.google.com/forms/d/e/1FAIpQLSfP9Crmf23fv0-00MuccKr7u957fedmCLRrViVzqcPSeMJPFw/viewform?usp=sf_link).

{{< button "/list-for-congo.txt" "Download the list" >}}

---

{{< grid "3" >}}
  {{< chart "dataset1" "table" >}}
{{< /grid >}}
