+++
title = "Home"
[data]
baseChartOn = 3
colors = ["#627c62", "#11819b", "#ef7f1a", "#4e1154"]
columnTitles = ["Section", "Status", "Author"]
fileLink = "content/projects.csv"
title = "Projects"
+++

{{< block "grid-2" >}}
{{< column >}}

# \#BLOCKOUT2024 with **Ease**.

Our main `goal` is to provide you the easiest experience in blocking accounts for the protest !

{{< tip >}}
You can install our extension and use it to `block` people/entities to contribute to the **boycott** ✊
{{< /tip >}}

{{< tip >}}
Spread the word among your friends and family so that more people can join the movement 📢
{{< /tip >}}


{{< tip >}}
Help us grow our blocklists by suggesting new accounts [here 📝](https://docs.google.com/forms/d/e/1FAIpQLSfP9Crmf23fv0-00MuccKr7u957fedmCLRrViVzqcPSeMJPFw/viewform?usp=sf_link)
{{< /tip >}}

{{< button "https://chromewebstore.google.com/detail/instagram-blocklist/efghnigapnojioilokgpbeekdcpkffcn" "Install the  extension" >}}{{< button "/install/how-to-use-the-blocker/" "How to use it ?" >}}
{{< /column >}}

{{< column >}}
![diy](/images/home/home_img.png)
{{< /column >}}
{{< /block >}}

